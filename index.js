function bai1() {
  var so1 = document.getElementById("txt-number1").value * 1;
  var so2 = document.getElementById("txt-number2").value * 1;
  var so3 = document.getElementById("txt-number3").value * 1;
  console.log(so1, so2, so3);
  var kq = "";
  if (so1 > so2) {
    if (so2 > so3) {
      kq += so3 + " " + so2 + " " + so1;
      document.getElementById("bai1").innerText = kq;
      // 3 2 1
    } else {
      if (so1 > so3) {
        kq += so2 + " " + so3 + " " + so1;
        document.getElementById("bai1").innerText = kq;
        // 2 3 1
      } else {
        kq += so2 + " " + so1 + " " + so3;
        document.getElementById("bai1").innerText = kq;
        // 2 1 3
      }
    }
  } else {
    if (so2 > so3) {
      if (so1 > so3) {
        kq += so3 + " " + so1 + " " + so2;
        document.getElementById("bai1").innerText = kq;
        // 3 1 2
      } else {
        kq += so1 + " " + so3 + " " + so2;
        document.getElementById("bai1").innerText = kq;
        // 1 3 2
      }
    } else {
      kq += so1 + " " + so2 + " " + so3;
      document.getElementById("bai1").innerText = kq;
      // 1 2 3
    }
  }
}

bai2 = () => {
  var bai2 = document.getElementById("selUser").value;
  if (bai2 == "B") {
    document.getElementById("bai2").innerText = "Xin Chào Bố!";
  }
  if (bai2 == "M") {
    document.getElementById("bai2").innerText = "Xin Chào Mẹ!";
  }
  if (bai2 == "A") {
    document.getElementById("bai2").innerText = "Xin Chào Anh Trai!";
  }
  if (bai2 == "E") {
    document.getElementById("bai2").innerText = "Xin Chào Em Gái!";
  }
};

bai3 = () => {
  so1 = document.getElementById("txt-so1").value * 1;
  so2 = document.getElementById("txt-so2").value * 1;
  so3 = document.getElementById("txt-so3").value * 1;
  let le = 0,
    chan = 0;
  if (so1 % 2 == 0) {
    chan++;
  } else {
    le++;
  }
  if (so2 % 2 == 0) {
    chan++;
  } else {
    le++;
  }
  if (so3 % 2 == 0) {
    chan++;
  } else {
    le++;
  }
  document.getElementById("bai3").innerText =
    le + " số lẻ và " + chan + " số chẵn";
};

bai4 = () => {
  canh1 = document.getElementById("txt-canh1").value * 1;
  canh2 = document.getElementById("txt-canh2").value * 1;
  canh3 = document.getElementById("txt-canh3").value * 1;
  if (
    canh1 * canh1 == canh2 * canh2 + canh3 * canh3 ||
    canh2 * canh2 == canh1 * canh1 + canh3 * canh3 ||
    canh3 * canh3 == canh1 * canh1 + canh2 * canh2
  ) {
    document.getElementById("bai4").innerText = "Tam giác vuông";
  }
  if (canh1 == canh2 && canh2 == canh3) {
    document.getElementById("bai4").innerText = "Tam giác đều";
  } else {
    if (canh1 == canh2 || canh2 == canh3 || canh3 == canh1) {
      document.getElementById("bai4").innerText = "Tam giác cân";
    }
  }
};
